import re
import unidecode


def clean_txt(x, sep=' ', abrev=False, max_size=None, cut='end', cut_join='(...)'):
    '''

    :param x: string or list of strings to be cleaned,if None returns None
    :param sep: word separator for the clean string by the end of the process when the string is formed by more than 1 words. DEFAULT = ' '
    :param abrev: If True access the abreviation excel and words within that excel are abreviated. DEFAULT = False
    :param max_size: Maximum number of characters the final string can have, when None the world returns completely.DEFAULT = None
    :param cut: Where the final string will be cut when max_size is defined, begin, middle or end. DEFAULT = 'end'
    :param cut_join: String or character to use to identify where a string was cut. DEFAULT = '(...)'
    :return: List ot cleaned strings
    '''
    if x is None:
        dataOut = None
        return dataOut

    if type(x) is str:
        x = [x]

    dataOut = []

    for w in x:
        w = str(w)
        w = w.lower()
        w = unidecode.unidecode(w)
        w = re.findall(r'[a-z0-9]+', w)
        if abrev:
            w = [i if i not in ABREV_DICT else ABREV_DICT[i] for i in w]
        w = sep.join([i.strip() for i in w])
        w = re.sub(r'([^a-z0-9])\1+', r'\1', w)

        if max_size is not None and len(w) > max_size:
            slc = len(w) - max_size
            if cut == 'end':
                w = w[:-slc].strip() + cut_join
            elif cut == 'begin':
                w = cut_join + w[slc:].strip()
            elif cut == 'middle':
                c_begin = round(max_size / 2)
                while w[c_begin] != sep:
                    c_begin+=1
                begin = w[:c_begin].strip()

                c_end = round(max_size / 2)
                while w[-c_end-1] != sep:
                    c_end+=1

                end = w[-c_end:].strip()
                w = begin + cut_join + end

        dataOut.append(w)

    return dataOut


def clean_decimal(x, get_number=True, decimals=2, decimal_sep=',', thousand_sep='.', abrev=None):
    '''

    :param x: string or numeric input
    :param get_number: if True returns a float number rounded as the number of decimals specified. DEFAULT = True
    :param decimals: Number of decimals to round the output. DEFAULT = 2
    :param decimal_sep: Separator character for the decimals. DEFAULT = ','
    :param thousand_sep: Separator character for the thousands. DEFAULT = '.'
    :param abrev: If K abreviates into thousands, If MI abreviates into millions else gets the number as it is. DEFAULT = None
    :return: Numeric or numeric like string
    '''
    if x is None:
        return None

    d = str(x)

    sep = re.findall(r'([^0-9])', d)

    if len(sep) > 0:
        n = re.findall(f'([0-9]+)', d)
        if len(n)>0:
            d = ''.join(n[:-1])+sep[-1]+n[-1]
        else:
            print(d)
            return None

        if get_number:
            d = float(d)
            d = round(d, decimals)
            return d

        if abrev == 'K':
            d = float(d) / 1000
            d = round(d, decimals)
            d = f'{d} K'
        elif abrev == 'MI':
            d = float(d) / 1000000
            d = round(d, decimals)
            d = f'{d} MI'
        else:
            d = round(float(d), decimals)

        if thousand_sep:
            d = str(d).split(sep[-1])
            d1 = format(int(d[0]), ',d').replace(',', thousand_sep)
            d = f'{d1}{decimal_sep}{d[1]}'

    return d


########################################################################################################################


ABREV_DICT = {
    "africa": "afr",
    "alquiler": "alq",
    "america": "amer",
    "analfabetas": "analf",
    "analfabetos": "analf",
    "anos": "anos",
    "asia": "asia",
    "bajo": "bajo",
    "caribe": "caribe",
    "casado": "cas",
    "cedida": "ced",
    "central": "cent",
    "civil": "civ",
    "compra": "comp",
    "con": "con",
    "de": "de",
    "del": "del",
    "dispone": "disp",
    "divorciado": "div",
    "donacion": "don",
    "en": "en",
    "entre": "entre",
    "es": "es",
    "espana": "esp",
    "espanola": "esp",
    "estado": "est",
    "estudios": "estud",
    "europeo": "eur",
    "extranjera": "ext",
    "grado": "grado",
    "gratis": "gratis",
    "habitacion": "hab",
    "habitaciones": "hab",
    "han": "han",
    "herencia": "heren",
    "hogares": "hog",
    "hombres": "hom",
    "incluido": "inc",
    "informacion": "info",
    "la": "la",
    "m2": "m2",
    "mas": "mas",
    "menores": "menor",
    "menos": "men",
    "miembro": "miembro",
    "mujeres": "muj",
    "nacido": "nac",
    "nacionalidad": "nacio",
    "nivel": "niv",
    "no": "no",
    "norte": "n",
    "o": "o",
    "oceania": "ocean",
    "otro": "otro",
    "pagada": "pagada",
    "pagos": "pagos",
    "pais": "pais",
    "pendientes": "pend",
    "persona": "pers",
    "personas": "pers",
    "poblacion": "pob",
    "por": "por",
    "precio": "prec",
    "primer": "prim",
    "principales": "princ",
    "propiedad": "prop",
    "que": "que",
    "regimen": "reg",
    "se": "se",
    "secundarias": "sec",
    "segundo": "seg",
    "separado": "sep",
    "sin": "sin",
    "sobre": "sobre",
    "soltero": "solt",
    "su": "su",
    "sur": "sur",
    "tenencia": "tenenc",
    "tercer": "terc",
    "tipo": "tip",
    "total": "tot",
    "totalmente": "tot",
    "ue": "ue",
    "un": "un",
    "vacias": "vacia",
    "viudo": "viudo",
    "viviendas": "viv",
    "y": "y"
}


########################################################################################################################


if __name__ == "__main__":
    print()