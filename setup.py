from setuptools import setup, find_packages

setup(
    name='tml_dataclean',
    version='1.1.10',
    description='Python snipets to clean and standardize numeric, text and time based data',
    url='https://gitlab.com/300mil/tml_dataclean',
    author='Andre Resende',
    author_email='dedsresende@gmail.com',
    license='unlicense',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Unidecode>=1.1.1'
    ]
)
